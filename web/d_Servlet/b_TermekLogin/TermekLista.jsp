<%-- 
    Document   : TermekLista
    Created on : Dec 17, 2014, 5:02:03 PM
    Author     : nbalazs
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <div align="right">
            <form action="LoginServlet">
                <c:choose>
                    <c:when test="${felhasznalo == null}">    
                        felhasználónév: 
                        <input type="text" name="felhasznalonev" value="bela2" />
                        jelszó:
                        <input type="password" name="jelszo" value="1234" />
                        <br/>
                        <c:if test="${hiba != null}">
                            <font color="red">${hiba}</font>
                        </c:if>
                        <input type="submit" value="Belép"/>
                    </c:when>
                    <c:otherwise>
                        Üdvözlüm ${felhasznalo.felhasznalonev}<br/>
                        <input type="submit" name="kilep" value="Kilépés" />
                    </c:otherwise>
                </c:choose>
            </form>
        </div>        
        <h1>Bolt!</h1>
        <div align="right">
            Kosár: 
            <c:choose>
                <c:when test="${kosar == null}">
                    0
                </c:when>
                <c:otherwise>
                    ${fn:length(kosar)}
                </c:otherwise>
            </c:choose>
            db elem
        </div>
        <c:if test="${termekLista == null}">
            <c:redirect url="TermekServlet" />
        </c:if>
        <form action="TermekServlet">
            <table border="1">
                <tr>
                    <th>Azonosító</th>
                    <th>Terméknév</th>
                    <th>Egységár</th>
                    <th>Készlet</th>
                    <th></th>
                </tr>
                <c:forEach items="${termekLista}" var="termek">
                    <tr>
                        <td>${termek.azon}</td>
                        <td>${termek.nev}</td>
                        <td>${termek.egysegar}</td>
                        <td>${termek.mennyiseg}</td>
                        <td><button type="submit" name="kosarba" value="${termek.azon}">Kosárba</button></td>
                    </tr>
                </c:forEach>
            </table>
        </form>
    </center>
</body>
</html>
