<%-- 
    Document   : TermekLista
    Created on : Dec 17, 2014, 5:02:03 PM
    Author     : nbalazs
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Bolt!</h1>
        <c:if test="${termekLista == null}">
            <c:redirect url="TermekServlet" />
        </c:if>
        <table border="1">
            <tr>
                <th>Azonosító</th>
                <th>Terméknév</th>
                <th>Egységár</th>
                <th>Készlet</th>
            </tr>
            <c:forEach items="${termekLista}" var="termek">
                <tr>
                    <td>${termek.azon}</td>
                    <td>${termek.nev}</td>
                    <td>${termek.egysegar}</td>
                    <td>${termek.mennyiseg}</td>
                </tr>
            </c:forEach>
        </table>
    </center>
    </body>
</html>
