<%-- 
    Document   : A_Feltolt
    Created on : Dec 13, 2014, 8:22:15 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Töltsd fel!</h1>
        <form action="A_Feltolt" method="post" enctype="multipart/form-data">
            Fájlnév: 
            <input type="file" 
                name="kep" accept="image/*"/>
            <input type="submit" value="Feltölt"/>
        </form>
        
    </body>
</html>
