<%-- 
    Document   : A_Feltolt
    Created on : Dec 13, 2014, 8:22:15 PM
    Author     : nbalazs
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Töltsd fel!</h1>
        <c:if test="${kepek == null}">
            <c:redirect url="A_Feltolt2" />
        </c:if>
        <form action="A_Feltolt2" method="post" enctype="multipart/form-data">
            Fájlnév: 
            <input type="file" 
                name="kep" accept="image/*"/>
            <input type="submit" value="Feltölt"/>
        </form>
        <c:forEach items="${kepek}" var="kep">
            <a href="${kep[1]}" target="blank"><img src="${kep[0]}"/></a>
        </c:forEach>
    </body>
</html>
