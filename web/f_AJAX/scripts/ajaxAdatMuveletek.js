//tabla keszites kódból
function getFejlec(fej) {
    var sor = "\t<tr>";
    for (var i = 0; i < fej.length; i++) {
        sor += "<th>" + fej[i] + "</th>";
    }
    sor += "</tr>\n";
    return(sor);
}

function getTorzs(sorok) {
    var torzs = "";
    for (var i = 0; i < sorok.length; i++) {
        torzs += " <tr>";
        var sor = sorok[i];
        for (var j = 0; j < sor.length; j++) {
            torzs += "<td>" + sor[j] + "</td>";
        }
        torzs += "</tr>\n";
    }
    return(torzs);
}

function getTabla(fej, sorok) {
    var tabla = "<table border='1'>\n" +
            getFejlec(fej) +
            getTorzs(sorok) +
            "</table>";
    return(tabla);
}

function tablaKeszit(cel) {
    var fej = ["Név", "Mennyiség", "Egységár"];
    var sorok = [
        ["Tej", "20", "230"],
        ["Kenyér", "30", "220"],
        ["Zsemle", "80", "20"],
        ["Kifli", "80", "20"],
    ];
    var tabla = getTabla(fej, sorok);
    document.getElementById(cel).innerHTML = tabla;
}

//xml kezelés
function xmlAdatok() {
    var txt = "<felsorolas>\n\
                <gyumolcs>Alma</gyumolcs>\n\
                <zoldseg>Repa</zoldseg>\n\
                <gyumolcs>Barack</gyumolcs>\n\
                <gyumolcs>Körta</gyumolcs>\n\
            </felsorolas>";
    return (new DOMParser()).parseFromString(txt, "text/xml");
}

function getXMLErtekek(doc, elemNev) {
    var elemekTombje = doc.getElementsByTagName(elemNev);
    var elemErtekek = new Array();
    for (var i = 0; i < elemekTombje.length; i++) {
        elemErtekek[i] = elemekTombje[i].firstChild.nodeValue;
    }
    return elemErtekek;
}

function getElemErtekek(elem, alElemNevek) {
    var values = new Array(alElemNevek.length);
    for (var i = 0; i < alElemNevek.length; i++) {
        var name = alElemNevek[i];
        var alElem = elem.getElementsByTagName(name)[0];
        values[i] = alElem.firstChild.nodeValue;
    }
    return(values);
}

function getLista(ertekTomb) {
    var lista = "<ul>\n";
    for (var i = 0; i < ertekTomb.length; i++) {
        lista += "<li>" + ertekTomb[i] + "</li>"
    }
    lista += "</ul>";
    return lista;
}

function listaKeszit(cel) {
    var doc = xmlAdatok();
    var ertekTomb = getXMLErtekek(doc, "gyumolcs");
    var lista = getLista(ertekTomb);
    document.getElementById(cel).innerHTML = lista;
}