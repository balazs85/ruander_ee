/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getRequestObject() {
    if (window.XMLHttpRequest) {
        return(new XMLHttpRequest());
    } else if (window.ActiveXObject) {
        return(new ActiveXObject("Microsoft.XMLHTTP"));
    } else {
        return(null);
    }
}

function ajaxUzenet(cim) {
    var request = getRequestObject();
    request.onreadystatechange =
            function () {
                valaszKeszit(request);
            };
    request.open("GET", cim, true);
    request.send(null);
}

function idToValue(paramId){
    return encodeURI(document.getElementById(paramId).value);
}

function ajaxUzenetParameterrel(cim, paramId) {
    var p = idToValue(paramId);
    cim = cim+"?p="+p;
    ajaxUzenet(cim);
}

function ajaxUzenetDOM(cim, hely) {
    var request = getRequestObject();
    request.onreadystatechange =
            function () {
                valaszKeszitDOM(request, hely);
            };
    request.open("GET", cim, true);
    request.send(null);
}

function ajaxPostUzenetDOM(cim, adat, hely) {
    var request = getRequestObject();
    request.onreadystatechange =
            function () {
                valaszKeszitDOM(request, hely);
            };
    request.open("POST", cim, true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(adat);
}

function ajaxPostUzenetDOMParameterrel(cim, hely, paramId) {
    var p = idToValue(paramId);
    var adat = paramId+"="+p;
    ajaxPostUzenetDOM(cim, adat, hely);
}

function valaszKeszit(request) {
    if (request.readyState == 4 && request.status == 200) {
        alert(request.responseText);
    }
}

function valaszKeszitDOM(request, hely) {
    if (request.readyState == 4 && request.status == 200) {
        document.getElementById(hely).innerHTML = request.responseText;
        //alert(request.responseText);
    }
}
