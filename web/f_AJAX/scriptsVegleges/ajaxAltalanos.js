//tiszta AJAX
function getRequestObject() {
    if (window.XMLHttpRequest) {
        return(new XMLHttpRequest());
    } else if (window.ActiveXObject) {
        return(new ActiveXObject("Microsoft.XMLHTTP"));
    } else {
        return(null);
    }
}

function ajaxPost(cim, adat, valaszFeldolgozo) {
    var request = getRequestObject();
    request.onreadystatechange =
            function () {
                valaszFeldolgozo(request);
            };
    request.open("POST", cim, true);
    request.setRequestHeader("Content-Type",
            "application/x-www-form-urlencoded");
    request.send(adat);
}

//XML
function getXMLErtekek(doc, elemNev) {
    var elemekTombje = doc.getElementsByTagName(elemNev);
    var elemErtekek = new Array();
    for (var i = 0; i < elemekTombje.length; i++) {
        elemErtekek[i] = elemekTombje[i].firstChild.nodeValue;
    }
    return elemErtekek;
}

function getElemErtekek(elem, alElemNevek) {
    var values = new Array(alElemNevek.length);
    for (var i = 0; i < alElemNevek.length; i++) {
        var name = alElemNevek[i];
        var alElem = elem.getElementsByTagName(name)[0];
        values[i] = alElem.firstChild.nodeValue;
    }
    return(values);
}

//táblakészítés
function getFejlec(fej) {
    var sor = "\t<tr>";
    for (var i = 0; i < fej.length; i++) {
        sor += "<th>" + fej[i] + "</th>";
    }
    sor += "</tr>\n";
    return(sor);
}

function getTorzs(sorok) {
    var torzs = "";
    for (var i = 0; i < sorok.length; i++) {
        torzs += " <tr>";
        var sor = sorok[i];
        for (var j = 0; j < sor.length; j++) {
            torzs += "<td>" + sor[j] + "</td>";
        }
        torzs += "</tr>\n";
    }
    return(torzs);
}

function getTabla(fej, sorok) {
    var tabla = "<table border='1'>\n" +
            getFejlec(fej) +
            getTorzs(sorok) +
            "</table>";
    return(tabla);
}
