function xmlTermekMegjelenito(request, hely) {
    if (request.readyState == 4 && request.status == 200) {
        var doc = request.responseXML;
        var fej = getXMLErtekek(doc, "fej");
        var termekek = doc.getElementsByTagName("termek");
        var sorok = new Array(termekek.length);
        var termekElemNevek = ["azon", "nev", "egysegar", "mennyiseg"];
        for (var i = 0; i < termekek.length; i++) {
            sorok[i] = getElemErtekek(termekek[i], termekElemNevek);
        }
        var tabla = getTabla(fej, sorok);
        document.getElementById(hely).innerHTML = tabla;
    }
}

function xmlTermekTabla(hely, paramId) {
    var cim = "TermekServlet";
    var p = encodeURI(document.getElementById(paramId).value);
    var adat = paramId + "=" + p + "&format=xml";
    ajaxPost(cim, adat, function (request) {
        xmlTermekMegjelenito(request, hely);
    });
}

