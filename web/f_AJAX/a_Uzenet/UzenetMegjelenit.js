/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getRequestObject() {
    if (window.XMLHttpRequest) {
        return(new XMLHttpRequest());
    } else if (window.ActiveXObject) {
        return(new ActiveXObject("Microsoft.XMLHTTP"));
    } else {
        return(null);
    }
}

function keresKuldese() {
    var request = getRequestObject();
    request.onreadystatechange =
            function () {
                valaszKeszit(request);
            };
    request.open("GET", "a_Uzenet.html", true);
    request.send(null);
}

function valaszKeszit(request) {
    if (request.readyState == 4) {
        alert(request.responseText);
    }
}
