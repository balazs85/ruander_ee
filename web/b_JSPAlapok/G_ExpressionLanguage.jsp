<%-- 
    Document   : F_JavaBeans
    Created on : Nov 22, 2014, 6:33:07 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="diak" class="a_JSPAlapok.F_DiakBean">
            <jsp:setProperty name="diak" property="nev" value="Béla" />
            <jsp:setProperty name="diak" property="magassag" value="10" />
        </jsp:useBean>

        <p>Név: <jsp:getProperty name="diak" property="nev" /></p>
        <p>Magasság: <jsp:getProperty name="diak" property="magassag" /></p>

        <p>Név: ${diak.nev}</p>
        <p>Magasság: ${diak.magassag}</p>
        
        <p>${header["user-agent"]}</p>
    </body>
</html>
