<%-- 
    Document   : H_JSTL
    Created on : Nov 22, 2014, 7:48:57 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set var="fizetes" value="${20000*2}" />
        <c:out value="${fizetes}" />
        
        <jsp:useBean id="diak" class="a_JSPAlapok.F_DiakBean" />
        <c:set value="Béla" target="${diak}" property="nev" />
        <c:set value="170" target="${diak}" property="magassag" />
        <c:out value="${diak.nev}" />
        <c:out value="${diak.magassag}" />
    </body>
</html>
