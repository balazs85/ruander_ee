<%-- 
    Document   : Termek
    Created on : Dec 5, 2014, 4:38:29 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Termék JSP</h1>
        <jsp:useBean id="egyTermek" class="b_JSPAlapok.F2_TermekBean" />
        <!--    Név-->
        <jsp:setProperty 
            name="egyTermek" 
            property="termeknev"
            value="<%= request.getParameter("termeknev")%>" />


        <!--    egységár-->
        <jsp:setProperty 
            name="egyTermek" 
            property="egysegar"
            value="<%= Integer.parseInt(request.getParameter("egysegar"))%>" />

        <!--    mennyiség-->

        <jsp:setProperty 
            name="egyTermek" 
            property="mennyiseg"
            value="<%= Integer.parseInt(request.getParameter("mennyiseg"))%>" />

        <table border="2">
            <tr>
                <th>Terméknév</th>
                <th>Mennyiség</th>
                <th>Egységár</th>
                <th>Végösszeg</th>
            </tr>
            <tr>
                <td><jsp:getProperty name="egyTermek" property="termeknev" /></td>
                <td><jsp:getProperty name="egyTermek" property="egysegar" /></td>
                <td><jsp:getProperty name="egyTermek" property="mennyiseg" /></td>
                <td><jsp:getProperty name="egyTermek" property="vegosszeg" /></td>
            </tr>
        </table>

    </center>
</body>
</html>
