<%-- 
    Document   : Termek
    Created on : Dec 5, 2014, 4:38:29 PM
    Author     : nbalazs
--%>

<%@page import="b_JSPAlapok.F2_TermekBean"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<%! ArrayList termekek = new ArrayList(); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Termék JSP</h1>
        <jsp:useBean id="egyTermek" class="b_JSPAlapok.F2_TermekBean" />

        <jsp:setProperty 
            name="egyTermek" 
            property="*" />
        
        <% termekek.add(egyTermek); %>
        
        <table border="2">
            <tr>
                <th>Terméknév</th>
                <th>Mennyiség</th>
                <th>Egységár</th>
                <th>Végösszeg</th>
            </tr>
            <% for(int i = 0; i < termekek.size(); i++) {%>
                <tr>
                    <td><%= ((F2_TermekBean)termekek.get(i)).getTermeknev() %></td>
                    <td><%= ((F2_TermekBean)termekek.get(i)).getEgysegar()%></td>
                    <td><%= ((F2_TermekBean)termekek.get(i)).getMennyiseg()%></td>
                    <td><%= ((F2_TermekBean)termekek.get(i)).getVegosszeg()%></td>
                </tr>
            <%}%>
        </table>
    </center>
</body>
</html>
