<%-- 
    Document   : Termek
    Created on : Dec 5, 2014, 4:38:29 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Termék JSP</h1>
        <jsp:useBean id="egyTermek" class="b_JSPAlapok.F2_TermekBean" />

        <!--Mindent egyben leveszünk az egyTermek-be. Ehhez persze a paramétereknek jó névvel kell rendelkezniük-->
        <jsp:setProperty 
            name="egyTermek" 
            property="*" />

        <table border="2">
            <tr>
                <th>Terméknév</th>
                <th>Mennyiség</th>
                <th>Egységár</th>
                <th>Végösszeg</th>
            </tr>
            <tr>
                <td><jsp:getProperty name="egyTermek" property="termeknev" /></td>
                <td><jsp:getProperty name="egyTermek" property="egysegar" /></td>
                <td><jsp:getProperty name="egyTermek" property="mennyiseg" /></td>
                <td><jsp:getProperty name="egyTermek" property="vegosszeg" /></td>
            </tr>
        </table>

    </center>
</body>
</html>
