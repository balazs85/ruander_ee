<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%! String[] elemek = {"Egy", "Kettő", "Három"}; %>
        <%! int i; %>

        <ul>
        <% for (i = 0; i < elemek.length; i++){%>
            <li><%= elemek[i] %></li>
        <%}%>
        </ul>
    </body>
</html>
