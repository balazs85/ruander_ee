<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  int i = 0; %>
        <%! int j = 0; %>
        
        <p><%= ++i %></p>
        <p><%= ++j %></p>
        
        <p>A mai nap: <%= (new java.util.Date()).toString()%></p>
    </body>
</html>
