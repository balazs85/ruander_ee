<%-- 
    Document   : F_JavaBeans
    Created on : Nov 22, 2014, 6:33:07 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="diak" class="b_JSPAlapok.F_DiakBean" />
        <jsp:setProperty name="diak" property="nev" value="Béla" />
        <jsp:setProperty name="diak" property="magassag" value="170" />

        <jsp:useBean id="diak2" class="b_JSPAlapok.F_DiakBean" />
        <% diak2.setNev("András");%>
        <% diak2.setMagassag(180);%>


        <p>Név: <jsp:getProperty name="diak" property="nev" /></p>
        <p>Magasság: <jsp:getProperty name="diak" property="magassag" /></p>
        
        <p>Név: <%= diak2.getNev() %></p>
        <p>Magasság: <%= diak2.getMagassag() %></p>
    </body>
</html>
