<%-- 
    Document   : A_JavaKodreszletben
    Created on : Dec 6, 2014, 11:07:28 AM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Véletlen oldalszín</title>
    </head>
    <% 
        String[] szinek = {"red", "green", "blue", 
            "azure", "gold", "greenyellow", "lavender"};
        String szin = szinek[(int)(Math.random()*szinek.length)];
    %>
    <body bgcolor="<%= szin %>">
        <h1>Hello World!</h1>
    </body>
</html>
