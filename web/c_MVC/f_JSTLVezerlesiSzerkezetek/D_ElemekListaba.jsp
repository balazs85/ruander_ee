<%-- 
    Document   : D_ElemekListaba
    Created on : Dec 11, 2014, 4:03:51 PM
    Author     : nbalazs
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% ArrayList l = new ArrayList(); %>
<%
        l.add("alma");
        l.add("körte");
        l.add("barack");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Adj elemet a listához!</h1>

        <ul>
            <c:forEach var="elem" items="${l}">
                <li>${elem}</li>
            </c:forEach>
        </ul>
    </body>
</html>
