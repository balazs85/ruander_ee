<%-- 
    Document   : HogyHivnak
    Created on : Dec 11, 2014, 12:38:10 PM
    Author     : nbalazs
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${(param.nev == null) || (param.nev.trim() == '')}">
            <h1>Add meg a neved!</h1>
            <c:if test="${param.nev.trim() == ''}">
                <p><font color="red">Nem írtál be nevet!</font></p>
                </c:if>
            <form>
                Név: <input type="text" name="nev" />
                <input type="submit" />
            </form>
        </c:if>
        <c:if test="${(param.nev != null) && (param.nev.trim() != '')}">
            <h1>Hello ${param.nev}!</h1>
        </c:if>    
    </body>
</html>
