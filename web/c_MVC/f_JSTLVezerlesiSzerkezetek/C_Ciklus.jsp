<%-- 
    Document   : c_Ciklus
    Created on : Dec 11, 2014, 2:38:49 PM
    Author     : nbalazs
--%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%! ArrayList l = new ArrayList(); %>
<%
    l.add("alma");
    l.add("körte");
    l.add("barack");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ciklus</title>
    </head>
    <body>
        <h1>Számok kettesével tízig</h1>
        <c:forEach var="i" begin="2" end="10" step="2">
            ${i}. szám<br/>
        </c:forEach>    

        <h1>Szöveg darabjai</h1>  
        <c:forTokens items="alma,körte,barack" delims="," var="gyumolcs">
            <c:out value="${gyumolcs}"/><br/>
        </c:forTokens>


        <jsp:useBean id="elemek" class="java.util.ArrayList">
            <%
                elemek.add("egyik");
                elemek.add("masik");
                elemek.add("harmadik");
            %>
        </jsp:useBean>    
        <h1>Lista elemeinek megjelenítése</h1>
        <c:forEach items="${elemek}" var="elem">
            <c:out value="${elem}"/><br/>
        </c:forEach>
    </body>
</html>
