<%-- 
    Document   : C_ELJSP2
    Created on : Dec 6, 2014, 7:26:16 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP EL</title>
    </head>
    <body>
        <center>
            <h1>Név</h1>
            <p>${nev.vezetek} ${nev["kereszt"]}</p>
            <p>${nev2[0]} ${nev2[1]}</p>
            <p>${nev3[0]} ${nev3[1]}</p>
            <p>${nev4.vezetek} ${nev4["kereszt"]}</p>
        </center>  
    </body>
</html>
