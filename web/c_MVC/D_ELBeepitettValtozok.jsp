<%-- 
    Document   : D_ELBeepitettValtozok
    Created on : Dec 6, 2014, 8:10:44 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <center>
            <h1>Beápített változók!</h1>
            <form>
                Név: <input type="text" name="nev" value="Béla" />
                <input type="submit" />
            </form>
        </center>
        <div>
            <p><B>A neved: </B>${param.nev}</p>
            <p><B>Böngésző: </B>${header["User-Agent"]}</p>
            <p><B>JSESSIONID Süti értéke: </B>${cookie.JSESSIONID.value}</p>
            <p><B>Szerver: </B>${pageContext.servletContext.serverInfo}</p>
        </div>
    </body>
</html>
