<%-- 
    Document   : Termek
    Created on : Dec 5, 2014, 4:38:29 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Termék JSP</h1>
        <table border="2">
            <tr>
                <th>Terméknév</th>
                <th>Mennyiség</th>
                <th>Egységár</th>
                <th>Végösszeg</th>
            </tr>
            <tr>
                <td>${termek.termeknev}</td>
                <td>${termek.egysegar}</td>
                <td>${termek.mennyiseg}</td>
                <td>${termek.vegosszeg}</td>
            </tr>
        </table>

    </center>
</body>
</html>
