<%-- 
    Document   : TermekUrlap
    Created on : Dec 5, 2014, 4:30:52 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Termék JSP meghívása</title>
    </head>
    <body>
        <center>
            <h1>Termék JSP meghívása</h1>
            <form action="../../A_TermekServlet">
                <table>
                    <tr>
                        <td>Termék neve:</td>
                        <td><input type="text" name="termeknev" value="zsömle"/></td>
                    </tr>
                    <tr>
                        <td>Termék egységára:</td>
                        <td><input type="text" name="egysegar" value="20"/></td>
                    </tr>
                    <tr>
                        <td>Vásárolt mennyiség:</td>
                        <td><input type="text" name="mennyiseg" value="5"/></td>
                    </tr>
                </table>
                <input type="submit" value="Kiszámol"/>
            </form>     
        </center>
    </body>
</html>
