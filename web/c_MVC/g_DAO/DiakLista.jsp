<%-- 
    Document   : DiakLista
    Created on : Dec 13, 2014, 9:38:53 AM
    Author     : nbalazs
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${diakok == null}">
            <c:redirect url="DiakMegjelenitServlet" />
        </c:if>
        <center>
            <h1>Diákok listája</h1>
            <p><font color="green">${uzenet}</font></p>
            <form action="DiakMegjelenitServlet" method="GET">
                <table border="1">
                    <tr>
                        <th>Azonosító</th>
                        <th>Név</th>
                        <th>Magasság</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <c:forEach var="diak" items="${diakok}" >
                    <tr>
                        <td>${diak.azon}</td>
                        <td>${diak.nev}</td>
                        <td>${diak.magassag}</td>
                        <td><button type="submit" name="modosit" value="${diak.azon}">Módosít</button></td>
                        <td><button type="submit" name="torol" value="${diak.azon}">Töröl</button></td>
                    </tr>
                    </c:forEach>
                </table>
                <button type="submit" name="modosit" value="-1">Új diák hozzáadása</button>
            </form>
            
        </center>
    </body>
</html>
