<%-- 
    Document   : DiakModosit
    Created on : Dec 13, 2014, 1:45:21 PM
    Author     : nbalazs
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Művelet</title>
    </head>
    <body>
    <center>
        <c:if test="${muvelet == 'modosit'}">
            <h1>${diak.azon} módosítása</h1>
        </c:if>
        <form action="DiakMegjelenitServlet" method="GET">
            <table border="1">
                <tr>
                    <td>Azonosító</td>
                    <td><input type="text" name="azon" value="${diak.azon}" readonly></td>
                </tr>
                <tr>
                    <td>Név</td>
                    <td><input type="text" name="nev" value="${diak.nev}"</td>
                </tr>
                <tr>
                    <td>Magasság</td>
                    <td><input type="text" name="magassag" value="${diak.magassag}"</td>
                </tr>
            </table>
                <button type="submit" name="muvelet" value="${muvelet}">Ment</button>
        </form>
    </center>
    </body>
</html>
