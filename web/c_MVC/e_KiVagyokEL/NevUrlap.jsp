<%-- 
    Document   : Urlap
    Created on : Dec 6, 2014, 6:28:35 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ki vagyok?</title>
    </head>
    <body>
        <center>
            <h1>Hogy hívnak?</h1>
            ${nev!=null && (nev.vezetek == "Nincs megadva" || nev.kereszt == "Nincs megadva") ? 
              "Nem adtad meg a teljes neved" : ""}
            <form action=${cim==null ? "../../E_NevServlet": "E_NevServlet"}>
                <table>
                    <tr>
                        <td>Vezetéknév:</td>
                        <td><input type="text" name="vezetek" 
                            value="${nev.vezetek == "Nincs megadva" ? "" : nev.vezetek}"/></td>
                    </tr>
                    <tr>
                        <td>Keresztnév:</td>
                        <td><input type="text" name="kereszt" 
                            value="${nev.kereszt == "Nincs megadva" ? "" : nev.kereszt}"/></td>
                    </tr>
                </table>
                <input type="submit" value="Megad"/>
            </form>     

        </center>
        
    </body>
</html>
