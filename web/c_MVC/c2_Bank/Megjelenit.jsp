<%-- 
    Document   : Megjelenit
    Created on : 2014.12.11., 19:33:13
    Author     : oktato2
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <h1>A kiválasztott ügyfél!</h1>
        <table border="1">
            <tr>
                <th>Azonosító</th>
                <th>Név</th>
                <th>Egyenleg</th>
            </tr>
            <tr>
                <td>${ugyfel.azon}</td>
                <td>${ugyfel.nev}</td>
                <td>${ugyfel.egyenleg}</td>
            </tr>
        </table>
    </body>
</html>
