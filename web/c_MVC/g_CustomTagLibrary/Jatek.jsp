<%-- 
    Document   : Jatek
    Created on : Dec 13, 2014, 6:47:13 PM
    Author     : nbalazs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/sajat-taglib.tld" prefix="sajat" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Játszunk!</title>
    </head>
    <body>
        <h1>Játszunk!</h1>
        <p><sajat:KoszonoTag>Béla</sajat:KoszonoTag></p>
        <p><sajat:KoszonoTag szin="red">András</sajat:KoszonoTag></p>
        <h2>Sima kocka</h2>
        <ul>
            <li><sajat:DobokockaTag/></li>
            <li><sajat:DobokockaTag/></li>
            <li><sajat:DobokockaTag/></li>
            <li><sajat:DobokockaTag/></li>
            <li><sajat:DobokockaTag/></li>
            <li><sajat:DobokockaTag/></li>
        </ul>
        <h2>12 oldalas kocka kocka</h2>
        <ul>
            <li><sajat:DobokockaTag oldal="12"/></li>
            <li><sajat:DobokockaTag oldal="12"/></li>
            <li><sajat:DobokockaTag oldal="12"/></li>
            <li><sajat:DobokockaTag oldal="12"/></li>
            <li><sajat:DobokockaTag oldal="12"/></li>
            <li><sajat:DobokockaTag oldal="12"/></li>
        </ul>
    </body>
</html>
