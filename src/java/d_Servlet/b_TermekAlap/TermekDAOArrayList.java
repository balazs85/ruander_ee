/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet.b_TermekAlap;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class TermekDAOArrayList implements TermekDAOInterface{
    
    List<Termek> termekek = new ArrayList<>();

    public TermekDAOArrayList() {
        termekek.add(new Termek(1, "Kenyér", 250, 100));
        termekek.add(new Termek(2, "Tej", 260, 100));
        termekek.add(new Termek(3, "Kifli", 20, 1000));
        termekek.add(new Termek(4, "Zsömle", 20, 1000));
        
    }
    
    @Override
    public List<Termek> getTermekek() {
        return termekek;
    }
    
}
