/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet.b_TermekKesz;

import d_Servlet.b_TermekAlap.*;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public interface TermekDAOInterface {
    public List<Termek> getTermekek();
    public Termek getTermek(Integer azon);
    
}
