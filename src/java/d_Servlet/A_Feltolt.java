/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "A_Feltolt", urlPatterns = {"/d_Servlet/A_Feltolt"})
@MultipartConfig
public class A_Feltolt extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Part fajlDarab = request.getPart("kep");
        String fajlnev = getFajlnev(fajlDarab);
        File cel = new File(getServletContext().getRealPath("/res/kepek/nagy")+"/"+fajlnev);
        InputStream fajlTartalom = fajlDarab.getInputStream();
        Files.copy(fajlTartalom, cel.toPath(),StandardCopyOption.REPLACE_EXISTING);

        File kisCel = new File(getServletContext().getRealPath("/res/kepek/belyeg")+"/"+fajlnev);
        Kep.kepAtmeretezese(cel, kisCel, 1024);
              
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getFajlnev(Part fajlDarab) {
        for (String s : fajlDarab.getHeader("content-disposition").split(";")) {
            System.out.println(s);
            if (s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=")+1).trim().replace("\"", "");
            }
        }
        return "kep.jpg";
    }

}
