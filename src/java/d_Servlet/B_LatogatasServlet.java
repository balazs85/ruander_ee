/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "B_LatogatasServlet", urlPatterns = {"/d_Servlet/B_LatogatasServlet"})
public class B_LatogatasServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        synchronized (session) {

            if (request.getParameter("torol") != null) {
                session.invalidate();
                response.getWriter().println("<a href='B_LatogatasServlet'>Újrakezd</a>");
            } else {

                String uzenet;
                Integer szamlalo = (Integer) session.getAttribute("szamlalo");
                if (szamlalo == null) {
                    szamlalo = 0;
                    uzenet = "Üdvözöllek új vendég az oldalamon!";
                } else {
                    uzenet = "Üdvözöllek újra az oldalamon!";
                    szamlalo = szamlalo + 1;
                }

                String id = session.getId();
                Date d = new Date(session.getCreationTime());
                Date ad = new Date(session.getLastAccessedTime());
                session.setAttribute("szamlalo", szamlalo);

                PrintWriter out = response.getWriter();
                out.println("<html>\n"
                        + "<h1>" + uzenet + "</h1>\n"
                        + "<table>\n"
                        + "<tr><td>Látogatásszám:</td><td>" + szamlalo + "</d></tr>"
                        + "<tr><td>Azonosító:</td><td>" + id + "</d></tr>"
                        + "<tr><td>Létrehozás:</td><td>" + d + "</d></tr>"
                        + "<tr><td>Hozzáférés:</td><td>" + ad + "</d></tr>"
                        + "</table>\n"
                        + "<form><button type=\"submit\" name=\"torol\">Session törlése</button></form>"
                        + "</html>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
