/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author nbalazs
 */
public class Kep {

    public static BufferedImage kepAtmeretezese(BufferedImage img, double ujNagyobbOldal) {
        int szelesseg = img.getWidth();
        int magassag = img.getHeight();
        double arany = ujNagyobbOldal / Math.max(magassag, szelesseg);
        int usz = (int) (arany * szelesseg);
        int um =  (int) (arany * magassag);
        BufferedImage dimg = new BufferedImage(usz, um, img.getType());
        Graphics2D g = dimg.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(img, 0, 0, usz, um, 0, 0, szelesseg, magassag, null);
        g.dispose();
        return dimg;
    }

    static void kepAtmeretezese(File cel, File kisCel, int ujNagyobbOldal) throws IOException {
        BufferedImage img = ImageIO.read(cel);
        BufferedImage kimg = Kep.kepAtmeretezese(img, ujNagyobbOldal);
        String p = cel.getAbsolutePath();
        String fajltipus = p.substring(p.lastIndexOf(".")+1);
        ImageIO.write(kimg, fajltipus, kisCel);
    }
}
