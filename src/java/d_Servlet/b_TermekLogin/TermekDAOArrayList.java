/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet.b_TermekLogin;

import d_Servlet.b_TermekKesz.*;
import d_Servlet.b_TermekAlap.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class TermekDAOArrayList implements TermekDAOInterface{
    
    List<Termek> termekek = new ArrayList<>();
    List<Felhasznalo> felhasznalok = new ArrayList<>();

    public TermekDAOArrayList() {
        termekek.add(new Termek(1, "Kenyér", 250, 100));
        termekek.add(new Termek(2, "Tej", 260, 100));
        termekek.add(new Termek(3, "Kifli", 20, 1000));
        termekek.add(new Termek(4, "Zsömle", 20, 1000));
        
        felhasznalok.add(new Felhasznalo("bela", "jelszo"));
        felhasznalok.add(new Felhasznalo("bela2", "1234"));
        
    }
    
    @Override
    public List<Termek> getTermekek() {
        return termekek;
    }

    @Override
    public Termek getTermek(Integer azon) {
       int i = 0;
        while (i<termekek.size() && termekek.get(i).getAzon()==azon){
            i++;
        }
        if (i<termekek.size()){
            return termekek.get(i);
        } else {
            return null;
        }
    }

    @Override
    public Felhasznalo login(String felhasznaloNev, String jelszo) {
        Felhasznalo f = new Felhasznalo(felhasznaloNev, jelszo);
        return login(f);
    }

    private Felhasznalo login(Felhasznalo f) {
        for (int i = 0; i < felhasznalok.size(); i++) {
            if (felhasznalok.get(i).equals(f)){
                return felhasznalok.get(i);
            }
        }
        return null;
    }
    
}
