/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet.b_TermekLogin;

import d_Servlet.b_TermekKesz.*;
import d_Servlet.b_TermekAlap.*;
import c_MVC.*;
import b_JSPAlapok.*;
import java.io.Serializable;

/**
 *
 * @author nbalazs
 */
public class Termek implements Serializable{
    private Integer azon;
    private String nev;
    private int egysegar;
    private int mennyiseg;

    public Termek(Integer azon, String nev, int egysegar, int mennyiseg) {
        this.azon = azon;
        this.nev = nev;
        this.egysegar = egysegar;
        this.mennyiseg = mennyiseg;
    }

    public Integer getAzon() {
        return azon;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public int getEgysegar() {
        return egysegar;
    }

    public int getMennyiseg() {
        return mennyiseg;
    }
    
    public int getVegosszeg(){
        return mennyiseg * egysegar;
    }
    
}
