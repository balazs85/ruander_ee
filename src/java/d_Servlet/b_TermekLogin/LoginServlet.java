/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet.b_TermekLogin;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/d_Servlet/b_TermekLogin/LoginServlet"})
public class LoginServlet extends HttpServlet {

    TermekDAOInterface adatbazis = new TermekDAOArrayList();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String felhasznalonev = request.getParameter("felhasznalonev");
        String jelszo = request.getParameter("jelszo");
        String kilep = request.getParameter("kilep");
        HttpSession session = request.getSession();
        Felhasznalo f = (Felhasznalo)session.getAttribute("felhasznalo");

        if (kilep != null){
            session.removeAttribute("felhasznalo");
        } else if (felhasznalonev != null && jelszo != null){
            f = adatbazis.login(felhasznalonev, jelszo);
            if (f != null){
                session.setAttribute("felhasznalo", f);
            } else {
                request.setAttribute("hiba", "Nincs ilyen felhasználó");
            }
        } else {
            request.setAttribute("hiba", "A felhasználónév, vagy jelszó nincs kitöltve");
        }
        //Mert a jsp azonnal a TermekServlethez menne, mert nincs kilistázva termék
        RequestDispatcher rd = request.getRequestDispatcher("TermekServlet");
        rd.forward(request, response);
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
