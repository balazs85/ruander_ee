/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet.b_TermekLogin;

import java.util.Objects;

/**
 *
 * @author nbalazs
 */
public class Felhasznalo {
    private String felhasznalonev;
    private String jelszo;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Felhasznalo other = (Felhasznalo) obj;
        if (!Objects.equals(this.felhasznalonev, other.felhasznalonev)) {
            return false;
        }
        if (!Objects.equals(this.jelszo, other.jelszo)) {
            return false;
        }
        return true;
    }

    public String getFelhasznalonev() {
        return felhasznalonev;
    }

    public String getJelszo() {
        return jelszo;
    }

    public Felhasznalo(String felhasznalonev, String jelszo) {
        this.felhasznalonev = felhasznalonev;
        this.jelszo = jelszo;
    }
}
