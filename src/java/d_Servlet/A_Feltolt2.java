/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_Servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "A_Feltolt2", urlPatterns = {"/d_Servlet/A_Feltolt2"})
@MultipartConfig
public class A_Feltolt2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String kepRelUt = "/res/kepek/";
        String kepGyoker = getServletContext().getRealPath(kepRelUt);
        String kepNagy = kepGyoker+"/nagy/";
        String kepBelyeg = kepGyoker+"/belyeg/";
        String htmlKepGyoker = ".."+kepRelUt;
        String htmlKepNagy = htmlKepGyoker+"/nagy/";
        String htmlKepBelyeg = htmlKepGyoker+"/belyeg/";
        
               
        try {
            Part fajlDarab = request.getPart("kep");
            String fajlnev = getFajlnev(fajlDarab);
            File cel = new File(kepNagy + fajlnev);
            InputStream fajlTartalom = fajlDarab.getInputStream();
            System.out.println(cel.toPath());
            Files.copy(fajlTartalom, cel.toPath(), StandardCopyOption.REPLACE_EXISTING);

            File kisCel = new File(kepBelyeg + fajlnev);
            Kep.kepAtmeretezese(cel, kisCel, 100);
        } catch (ServletException servletException) {
            if (!servletException.getMessage().equals("The request content-type is not a multipart/form-data")) {
                throw servletException;
            }
        }

        File mappa = new File(kepBelyeg);
        File[] dirList = mappa.listFiles();
        List<String[]> kepek = new ArrayList<>();
        for (File f : dirList) {
            String p = f.getPath();
            String fajlnev = p.substring(p.lastIndexOf("/") + 1);
            System.out.println(fajlnev);
            kepek.add(new String[]{htmlKepBelyeg + fajlnev, htmlKepNagy + fajlnev});
        }
        request.setAttribute("kepek", kepek);
        String cim = "A_Feltolt2.jsp";
        RequestDispatcher rd = request.getRequestDispatcher(cim);
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getFajlnev(Part fajlDarab) {
        for (String s : fajlDarab.getHeader("content-disposition").split(";")) {
            System.out.println(s);
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 1).trim().replace("\"", "");
            }
        }
        return "kep.jpg";
    }

}
