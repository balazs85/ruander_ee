/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a_Servlet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author nbalazs
 */
public class MyLog {

    private static String nev = "log.csv";

    public static void Log(HttpServlet s, HttpServletRequest request) {
        String p = s.getServletContext().getRealPath("/");
        File f = new File(p + nev);

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
            String sz = new Date().toString() + "\t";
            sz += request.getHeader("user-agent") + "\t";
            if (request.getParameter("nev") != null) {
                sz += request.getParameter("nev") + "\t";
            }
            out.println(sz);
            out.close();
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }

    }

    public static void Log(HttpServletRequest request, String p) {
        File f = new File(p + "../../res/" + nev);

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
            String sz = new Date().toString() + "\t";
            sz += request.getHeader("user-agent") + "\t";
            if (request.getParameter("nev") != null) {
                sz += request.getParameter("nev") + "\t";
            }
            out.println(sz);
            out.close();
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
    }

}
