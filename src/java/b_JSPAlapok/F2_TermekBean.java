/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b_JSPAlapok;

import java.io.Serializable;

/**
 *
 * @author nbalazs
 */
public class F2_TermekBean implements Serializable{
    private String termeknev;
    private int egysegar;
    private int mennyiseg;

    public F2_TermekBean() {
    }

    public String getTermeknev() {
        return termeknev;
    }

    public void setTermeknev(String termeknev) {
        this.termeknev = termeknev;
    }

    public int getEgysegar() {
        return egysegar;
    }

    public void setEgysegar(int egysegar) {
        this.egysegar = egysegar;
    }

    public int getMennyiseg() {
        return mennyiseg;
    }

    public void setMennyiseg(int mennyiseg) {
        this.mennyiseg = mennyiseg;
    }
    
    public int getVegosszeg(){
        return mennyiseg * egysegar;
    }
    
}
