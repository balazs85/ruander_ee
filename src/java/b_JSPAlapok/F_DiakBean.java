/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b_JSPAlapok;

import java.io.Serializable;

/**
 *
 * @author nbalazs
 */
public class F_DiakBean implements Serializable{
    private String nev;
    private int magassag;

    public F_DiakBean() {
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public int getMagassag() {
        return magassag;
    }

    public void setMagassag(int magassag) {
        this.magassag = magassag;
    }
}
