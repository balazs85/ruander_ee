/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f_AJAX.i_Termek;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class TermekDAOArrayList implements TermekDAOInterface {

    List<Termek> termekek = new ArrayList<>();

    public TermekDAOArrayList() {
        termekek.add(new Termek(1, "Kenyér", 250, 100));
        termekek.add(new Termek(2, "Tej", 260, 100));
        termekek.add(new Termek(3, "Kifli", 20, 1000));
        termekek.add(new Termek(4, "Zsömle", 20, 1000));

    }

    @Override
    public List<Termek> getTermekek() {
        return termekek;
    }

    @Override
    public Termek getTermek(Integer azon) {
        int i = 0;
        while (i < termekek.size() && termekek.get(i).getAzon() == azon) {
            i++;
        }
        if (i < termekek.size()) {
            return termekek.get(i);
        } else {
            return null;
        }
    }

    private static class nevComparator implements Comparator<Termek> {

        @Override
        public int compare(Termek o1, Termek o2) {
            return o1.getNev().compareTo(o2.getNev());
        }
    }
    private static class egysegarComparator implements Comparator<Termek> {

        @Override
        public int compare(Termek o1, Termek o2) {
            return o1.getEgysegar()-o2.getEgysegar();
        }
    }

    @Override
    public List<Termek> getTermekekRendezve(String szerint) {
        List<Termek> masolat = new ArrayList<Termek>(termekek);
        switch (szerint){
            case "nev_No": masolat.sort(new nevComparator()); break;
            case "egysegar_No": masolat.sort(new egysegarComparator()); break;   
        }
        return masolat;
    }

}
