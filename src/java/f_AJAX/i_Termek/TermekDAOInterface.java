/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f_AJAX.i_Termek;

import java.util.List;

/**
 *
 * @author nbalazs
 */
public interface TermekDAOInterface {
    public List<Termek> getTermekek();
    public Termek getTermek(Integer azon);
    public List<Termek> getTermekekRendezve(String szerint);
}
