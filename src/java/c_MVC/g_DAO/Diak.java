/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.g_DAO;

import java.io.Serializable;

/**
 *
 * @author nbalazs
 */
public class Diak implements Serializable{
    private Integer azon;
    private String nev;
    private Integer magassag;

    public Diak(Integer azon, String nev, Integer magassag) {
        this.azon = azon;
        this.nev = nev;
        this.magassag = magassag;
    }

    public Integer getAzon() {
        return azon;
    }

    public void setAzon(Integer azon) {
        this.azon = azon;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public Integer getMagassag() {
        return magassag;
    }

    public void setMagassag(Integer magassag) {
        this.magassag = magassag;
    }
    
}
