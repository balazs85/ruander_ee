/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.g_DAO;

import java.util.List;

/**
 *
 * @author nbalazs
 */
public interface DiakDAOInterface {
    public List<Diak> getOsszesDiak();
    public Diak getDiak(Integer azon);
    public void insertDiak(Diak d);
    public void updateDiak(Diak d);
    public void deleteDiak(Diak d);
    public Integer kovAzonosito();
}
