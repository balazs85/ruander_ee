/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.g_DAO;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "DiakMegjelenitServlet", urlPatterns = {"/c_MVC/g_DAO/DiakMegjelenitServlet"})
public class DiakMegjelenitServlet extends HttpServlet {

    DiakDAOInterface adatbazis = new DiakDAOApacheDerby();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        String muvelet = request.getParameter("muvelet");
        String modositAzonSzoveg = request.getParameter("modosit");
        Integer modositAzon;
        String torolAzonSzoveg = request.getParameter("torol");
        Integer torolAzon;
        String cim = "Hiba.jsp";
        Diak d = null;

        if (modositAzonSzoveg != null && modositAzonSzoveg.trim().equals("")) {
            request.setAttribute("hiba", "Nem adott meg diákazonosítót!");
        } else if ((modositAzon = szovegbolSzam(modositAzonSzoveg)) != null) {
            if (modositAzon == -1) {
                d = new Diak(adatbazis.kovAzonosito(), "", 0);
                request.setAttribute("diak", d);
                request.setAttribute("muvelet", "uj");
                cim = "DiakModosit.jsp";
            } else if ((d = adatbazis.getDiak(modositAzon)) == null) {
                request.setAttribute("hiba", "Nincs ilyen diák");
            } else {
                request.setAttribute("diak", d);
                request.setAttribute("muvelet", "modosit");
                cim = "DiakModosit.jsp";
            }
        } else if (torolAzonSzoveg != null && torolAzonSzoveg.trim().equals("")) {
            request.setAttribute("hiba", "Nem adott meg diákazonosítót a törléshez!");
        } else if ((torolAzon = szovegbolSzam(torolAzonSzoveg)) != null) {
            if ((d = adatbazis.getDiak(torolAzon)) == null) {
                request.setAttribute("hiba", "Nincs ilyen diák, akit kitörölhetnék");
            } else {
                adatbazis.deleteDiak(d);
                request.setAttribute("uzenet", "A diákot sikeresen kitöröltük!");
                List<Diak> diakok = adatbazis.getOsszesDiak();
                request.setAttribute("diakok", diakok);
                cim = "DiakLista.jsp";

            }
        } else if (muvelet != null) {
            if (muvelet.trim().equals("")) {
                request.setAttribute("hiba", "Nem adott meg műveletet!");
            } else {
                String azonSzoveg = request.getParameter("azon");
                Integer azon;
                String nev = request.getParameter("nev");
                String magassagSzoveg = request.getParameter("magassag");
                Integer magassag;
                if (azonSzoveg != null && !azonSzoveg.trim().equals("") && (azon = szovegbolSzam(azonSzoveg)) != null
                        && nev != null && !azonSzoveg.trim().equals("")
                        && magassagSzoveg != null && !magassagSzoveg.trim().equals("") && (magassag = szovegbolSzam(magassagSzoveg)) != null) {
                    d = new Diak(azon, nev, magassag);
                    switch (muvelet) {
                        case "modosit":
                            adatbazis.updateDiak(d);
                            request.setAttribute("uzenet", "A diákot módosítottuk!");
                            break;
                        case "uj":
                            adatbazis.insertDiak(d);
                            request.setAttribute("uzenet", "A diákot beszúrtuk!");
                            break;
                        default:
                            request.setAttribute("uzenet", "Nem jó műveletet adott meg!");
                    }
                    List<Diak> diakok = adatbazis.getOsszesDiak();
                    request.setAttribute("diakok", diakok);
                    cim = "DiakLista.jsp";
                } else {
                    request.setAttribute("hiba", "Rosszul adta meg az adatokat "
                            + azonSzoveg + ", " + nev + ", " + magassagSzoveg);
                }
            }
        } else {
            List<Diak> diakok = adatbazis.getOsszesDiak();
            request.setAttribute("diakok", diakok);
            cim = "DiakLista.jsp";
        }
        RequestDispatcher rd = request.getRequestDispatcher(cim);
        rd.forward(request, response);

    }

    private Integer szovegbolSzam(String egysegarSzoveg) {
        try {
            Integer eredmeny = Integer.valueOf(egysegarSzoveg);
            return eredmeny;
        } catch (NumberFormatException numberFormatException) {
            return null;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
