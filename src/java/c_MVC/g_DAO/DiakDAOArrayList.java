/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.g_DAO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class DiakDAOArrayList implements DiakDAOInterface{

    private List<Diak> diakok;

    public DiakDAOArrayList() {
        diakok = new ArrayList<>();
        insertDiak(new Diak(1, "Béla", 170));
        insertDiak(new Diak(2, "András", 180));
        insertDiak(new Diak(3, "Lujza", 173));
    }
    
    @Override
    public List<Diak> getOsszesDiak() {
        return diakok;
    }

    @Override
    public Diak getDiak(Integer azon) {
        int i = 0;
        while (i<diakok.size() && diakok.get(i).getAzon()==azon){
            i++;
        }
        if (i<diakok.size()){
            return diakok.get(i);
        } else {
            return null;
        }
    }

    @Override
    public void insertDiak(Diak d) {
        diakok.add(d);
    }

    @Override
    public void updateDiak(Diak d) {
        Diak dRegi = getDiak(d.getAzon());
        diakok.set(diakok.indexOf(dRegi), d);
    }

    @Override
    public void deleteDiak(Diak d) {
        Diak dRegi = getDiak(d.getAzon());
        diakok.remove(dRegi);
    }

    @Override
    public Integer kovAzonosito() {
        return diakok.get(diakok.size()-1).getAzon()+1;
    }
}
