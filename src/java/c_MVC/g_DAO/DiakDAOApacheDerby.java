/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.g_DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class DiakDAOApacheDerby implements DiakDAOInterface {

    private Connection kapcsolodas() {
        String user = "DiakAdmin";
        String pass = "titok";
        String url = "jdbc:derby://localhost:1527/Diak";
        Connection conn = null;

        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException classNotFoundException) {
        } catch (InstantiationException instantiationException) {
        } catch (IllegalAccessException illegalAccessException) {
        } catch (SQLException sQLException) {
        }
        return conn;
    }

    public DiakDAOApacheDerby() {

    }

    @Override
    public List<Diak> getOsszesDiak() {
        List<Diak> diakok = new ArrayList<>();
        try {
            Connection c = kapcsolodas();
            PreparedStatement stmt = c.prepareStatement("SELECT * FROM diak");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Integer azon = rs.getInt("azon");
                String nev = rs.getString("nev");
                Integer magassag = rs.getInt("magassag");
                diakok.add(new Diak(azon, nev, magassag));
            }
            c.close();
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        }
        return diakok;
    }

    @Override
    public Diak getDiak(Integer azon) {
        Diak d = null;
        try {
            Connection c = kapcsolodas();
            PreparedStatement stmt = c.prepareStatement(
                      "SELECT * FROM diak "
                    + "WHERE azon = ?");
            stmt.setInt(1, azon);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                String nev = rs.getString("nev");
                Integer magassag = rs.getInt("magassag");
                d = new Diak(azon, nev, magassag);
            }
            c.close();
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        }
        return d;
    }

    @Override
    public void insertDiak(Diak d) {
        try {
            Connection c = kapcsolodas();
            PreparedStatement stmt = c.prepareStatement(
                      "INSERT INTO diak VALUES (?, ?, ?)");
            stmt.setInt(1, d.getAzon());
            stmt.setString(2, d.getNev());
            stmt.setInt(3, d.getMagassag());
            stmt.executeUpdate();
            stmt.close();
            c.close();
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        }
    }

    @Override
    public void updateDiak(Diak d) {
        try {
            Connection c = kapcsolodas();
            PreparedStatement stmt = c.prepareStatement(
                      "UPDATE diak SET "
                    + "nev=?, "
                    + "magassag=?"
                    + "WHERE azon=?");
            stmt.setString(1, d.getNev());
            stmt.setInt(2, d.getMagassag());
            stmt.setInt(3, d.getAzon());
            stmt.executeUpdate();
            stmt.close();
            c.close();
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        }        
    }

    @Override
    public void deleteDiak(Diak d) {
        try {
            Connection c = kapcsolodas();
            PreparedStatement stmt = c.prepareStatement(
                      "DELETE FROM diak "
                    + "WHERE azon=?");
            stmt.setInt(1, d.getAzon());
            stmt.executeUpdate();
            stmt.close();
            c.close();
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        }
    }

    @Override
    public Integer kovAzonosito() {
        Integer max = 0;
        try {
            Connection c = kapcsolodas();
            PreparedStatement stmt = c.prepareStatement(
                      "SELECT MAX(azon) FROM diak ");
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                max = rs.getInt(1);
            }
            c.close();
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        }
        return max+1;
        
    }
}
