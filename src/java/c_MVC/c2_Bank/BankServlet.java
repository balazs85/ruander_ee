/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.c2_Bank;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author oktato2
 */
@WebServlet(name = "BankServlet", urlPatterns = {"/BankServlet"})
public class BankServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        BankUgyfelek.mintaugyfelekHozzadasa();
        
        String azon = request.getParameter("azon");
        String cim = "c_MVC/c2_Bank/Hiba.jsp";
        BankUgyfel u;

        
        if (azon == null || azon.trim().equals("")) {
            request.setAttribute("hiba", "Nem töltötte ki az azonosítót!");
        } else if ((u = BankUgyfelek.keres(azon))==null){
            request.setAttribute("hiba", "Nincs ilyen ügyfél!");
        } else {
            if (u.getEgyenleg() < 0){
                cim = "c_MVC/c2_Bank/Fenyeget.jsp";
            } else {
                cim = "c_MVC/c2_Bank/Megjelenit.jsp";
            }
            request.setAttribute("ugyfel", u);
        }
        
        RequestDispatcher rd = request.getRequestDispatcher(cim);
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
