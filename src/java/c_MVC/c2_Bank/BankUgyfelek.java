/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.c2_Bank;

import java.util.ArrayList;

/**
 *
 * @author oktato2
 */
public class BankUgyfelek {
    private static ArrayList<BankUgyfel> ugyfelek = new ArrayList<BankUgyfel>();
    
    public static void ugyfelHozzaad(BankUgyfel u){
        ugyfelek.add(u);
    }
    
    public static void ugyfelHozzaad(String azon, String nev, Integer egyenleg){
        ugyfelHozzaad(new BankUgyfel(azon, nev, egyenleg));
    }
    
    public static void mintaugyfelekHozzadasa(){
        if (ugyfelek.isEmpty()){
            ugyfelHozzaad("1", "Béla", 100000);
            ugyfelHozzaad("2", "Anrás", -1000);
            ugyfelHozzaad("3", "Anna", 0);
        }
    }

    public static BankUgyfel keres(String azon) {
        int i = 0;
        while (i<ugyfelek.size() && !ugyfelek.get(i).getAzon().equals(azon)){
            i++;
        }
        if (i<ugyfelek.size()){
            return ugyfelek.get(i);
        } else {
            return null;
        }
    }
}
