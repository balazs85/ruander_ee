/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "A_TermekServlet", urlPatterns = {"/A_TermekServlet"})
public class A_TermekServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            request.setCharacterEncoding("UTF-8");
            String termeknev = request.getParameter("termeknev");
            String egysegarSzoveg = request.getParameter("egysegar");
            String mennyisegSzoveg = request.getParameter("mennyiseg");
            Integer egysegar;
            Integer mennyiseg;
            String cim = "c_MVC/a_Termek/TermekHiba.jsp";
            if (termeknev == null || termeknev.equals("")){
                request.setAttribute("hiba", "Nincs kitöltve a név!");
            } else if (egysegarSzoveg == null || egysegarSzoveg.equals("")){
                request.setAttribute("hiba", "Nincs kitöltve az egységár!");
            } else if ((egysegar = szovegbolSzam(egysegarSzoveg)) == null){
                request.setAttribute("hiba", "Nem egészt írt az egységárhoz!");                
            } else if (mennyisegSzoveg == null || mennyisegSzoveg.equals("")){
                request.setAttribute("hiba", "Nincs kitöltve a mennyiség!");
            } else if ((mennyiseg = szovegbolSzam(mennyisegSzoveg)) == null){
                request.setAttribute("hiba", "Nem egészt írt a mennyiséghez!");                
            } else {
                A_TermekBean tb = new A_TermekBean(termeknev, egysegar, mennyiseg);
                request.setAttribute("termek", tb);
                cim = "c_MVC/a_Termek/Termek.jsp";
            }

            RequestDispatcher rd = request.getRequestDispatcher(cim);
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private Integer szovegbolSzam(String egysegarSzoveg) {
        try {
            Integer eredmeny = Integer.valueOf(egysegarSzoveg);
            return eredmeny;
        } catch (NumberFormatException numberFormatException) {
            return null;
        }
    }

}
