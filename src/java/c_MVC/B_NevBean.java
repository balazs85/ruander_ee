/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC;

/**
 *
 * @author nbalazs
 */
public class B_NevBean {

    private String vezetek;
    private String kereszt;

    public B_NevBean() {
        this.vezetek = "Nincs megadva";
        this.kereszt = "Nincs megadva";
    }

    public String getVezetek() {
        return vezetek;
    }

    public String getKereszt() {
        return kereszt;
    }

    public void setVezetek(String vezetek) {
        if (!hianyzik(vezetek)) {
            this.vezetek = vezetek;
        }
    }

    public void setKereszt(String kereszt) {
        if (!hianyzik(kereszt)) {
            this.kereszt = kereszt;
        }
    }

    private boolean hianyzik(String szoveg) {
        return (szoveg == null || szoveg.trim().equals(""));
    }

}
