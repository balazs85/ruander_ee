/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC;

import b_JSPAlapok.*;
import java.io.Serializable;

/**
 *
 * @author nbalazs
 */
public class A_TermekBean implements Serializable{
    private String termeknev;
    private int egysegar;
    private int mennyiseg;

    public A_TermekBean(String termeknev, int egysegar, int mennyiseg) {
        this.termeknev = termeknev;
        this.egysegar = egysegar;
        this.mennyiseg = mennyiseg;
    }

    public String getTermeknev() {
        return termeknev;
    }

    public int getEgysegar() {
        return egysegar;
    }

    public int getMennyiseg() {
        return mennyiseg;
    }
    
    public int getVegosszeg(){
        return mennyiseg * egysegar;
    }
    
}
