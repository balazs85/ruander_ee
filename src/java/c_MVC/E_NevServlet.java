/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nbalazs
 */
@WebServlet(name = "E_NevServlet", urlPatterns = {"/E_NevServlet"})
public class E_NevServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
                String cim = "/c_MVC/e_KiVagyokEL/Nev.jsp";
                B_NevBean nevBean = (B_NevBean) request.getAttribute("nev");
                if (nevBean == null) {
                    nevBean = new B_NevBean();
                    request.setAttribute("nev", nevBean);
                }
                nevBean.setVezetek(request.getParameter("vezetek"));
                nevBean.setKereszt(request.getParameter("kereszt"));
                if (nevBean.getKereszt().equals("Nincs megadva") || 
                    nevBean.getVezetek().equals("Nincs megadva")){
                    cim = "/c_MVC/e_KiVagyokEL/NevUrlap.jsp"; 
                }/* else {
                    cim = "/c_MVC/e_KiVagyokEL/Nev.jsp";
                }*/
//                System.out.println(cim);
                request.setAttribute("cim", "van");
                RequestDispatcher dispatcher = request.getRequestDispatcher(cim);
//                RequestDispatcher dispatcher = request.getRequestDispatcher("index.html");
                dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
