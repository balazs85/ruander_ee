/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_MVC.g_CustomTagLibrary;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author nbalazs
 */
public class DobokockaTag extends SimpleTagSupport {

    private int oldal = 6;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();

        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");
            int szam = ((int) (Math.random() * oldal)) + 1;
            out.print(szam);
            JspFragment f = getJspBody();
            if (f != null) {
                f.invoke(out);
            }
            
            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in DobokockaTag tag", ex);
        }
    }

    public void setOldal(String oldal) {
        try {
            this.oldal = Integer.parseInt(oldal);
        } catch (NumberFormatException e) {
            this.oldal = 6;
        }
    }


}
